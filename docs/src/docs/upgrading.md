# Upgrading

## Update to the Latest

To update Meltano to the latest version, run the following command in your terminal:

```
pip install --upgrade meltano
```